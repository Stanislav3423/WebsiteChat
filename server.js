const users = [];
const connections = [];
const io = require('socket.io')(3000, {
    cors: {
        origin: "http://192.168.0.100",
    },
});
io.on('connection', (socket) => {
    connections.push(socket);

    socket.on('disconnect', () => {
        users.splice(users.indexOf(socket.username), 1);
        io.sockets.emit('get user', users);
        connections.splice(connections.indexOf(socket), 1);
    });

    socket.on('send message', (data, info) => {
        if (info === "Group") {
            io.sockets.emit('new message', { msg: data, user: socket.username, chat: info });
        } else {
            connections[users.indexOf(info)].emit('new message', { msg: data, user: socket.username, chat: socket.username });
            socket.emit('new message', { msg: data, user: socket.username, chat: info });
        }
    });

    socket.on('new user', (data) => {
        socket.username = data;
        users.push(data);
        io.sockets.emit('get user', users);
    });
});