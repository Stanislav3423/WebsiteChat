const socket = io('http://192.168.0.100:3000');

const userForm = document.getElementById('userForm');
const username = document.getElementById('username');
const chatArea = document.getElementById('chatArea');
const users = document.getElementById('users');
const chatInfo = document.getElementById('chatInfo');
const chat = document.getElementById('chat');
const messageForm = document.getElementById('messageForm');
const input = document.getElementById('message');
const userBlock = document.getElementById('userBlock');

let choosenChat = "Group";
let user;
const allChats = {};
allChats["Group"] = [];

messageForm.addEventListener('submit', (e) => {
    e.preventDefault();
    if (input.value && user) {
        socket.emit('send message', input.value, choosenChat);
        input.value = '';
    }
});

userForm.addEventListener('submit',(e)=>{
    e.preventDefault();
    if(username.value){
        socket.emit('new user', username.value);
        user = username.value;
        chatArea.style.display = 'flex';
        userBlock.style.display = 'none';
    }
});

socket.on('new message', (data) => {
    if (!(data.chat in allChats)) {
        allChats[data.chat] = [];
    }
    allChats[data.chat].push({ user: data.user, msg: data.msg });
    updateChatHTML();
});

function updateChatHTML() {
    let html = '';
    for (let i = 0; i < allChats[choosenChat]?.length; i++){
        html += `<div class ="mb-2 d-flex align-items-center">
            <img src="message_icon.png" class="rounded-circle me-1">
            <div class="message-block">
                <span class="user message-title-style">${allChats[choosenChat][i].user}</span><br>
                <span class="message">${allChats[choosenChat][i].msg}</span>
            </div>
            </div>`;
    }
    chat.innerHTML = html;
    chat.scrollTop = chat.scrollHeight;
}

socket.on('get user', (data) => {
    let html = '<div class="list-group-item mt-2"><img src="message_icon.png" class="rounded-circle me-2">Group</div>';
    for (let i = 0; i < data.length; i++){
        if (data[i] !== user) {
            html += `<div class="list-group-item mt-2">
                <img src="message_icon.png" class="rounded-circle">
                ${data[i]}</div>`;
        }
    }
    users.innerHTML = html;
    chat.scrollTop = chat.scrollHeight;
});

users.addEventListener('click', (e) => {
    if (e.target.classList.contains('list-group-item')) {
        chooseChat(e.target);
    }
});

function chooseChat(item) {
    choosenChat = item.textContent.trim();
    chatInfo.innerHTML = "Chat room " + choosenChat;
    updateChatHTML();
}